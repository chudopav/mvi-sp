import torch
from torch import nn
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import argparse
from sklearn.preprocessing import MinMaxScaler
from statsmodels.datasets import get_rdataset
import random
import json

from ntm.ntm import NTM

parser = argparse.ArgumentParser()
parser.add_argument("--min_seq_len", type=int, default=10)
parser.add_argument("--max_seq_len", type=int, default=20)
parser.add_argument('--optimizer', type=str, default='RMSProp', help='RMSProp | Adam | AdamW')
parser.add_argument('--learning_rate', type=float, default=5e-5)
parser.add_argument('--num_epochs', type=int, default=2000)
parser.add_argument('--max_grad_norm', type=float, default=20.)
parser.add_argument('--eval_frequency', type=int, default=50)
parser.add_argument('--hidden_dim', type=int, default=100)
parser.add_argument('--controller_layers', type=int, default=1)
parser.add_argument('--controller_clipping', type=float, default=20.)
parser.add_argument('--memory_size', type=int, default=128)
parser.add_argument('--memory_cell_size', type=int, default=20)
parser.add_argument('--memory_init_weights', type=str, default='ones', help='ones | learned | random')
parser.add_argument('--num_heads', type=int, default=1)
parser.add_argument('--max_shift', type=int, default=1)
parser.add_argument('--dropout_rate', type=int, default=0.3)
parser.add_argument('--visualize_memory', type=bool, default=False)
parser.add_argument('--visualize_prediction', type=bool, default=False)
parser.add_argument('--device', type=str, default='cuda')
parser.add_argument('--train_num_sequences', type=int, default=25)
parser.add_argument('--pred_seq_len', type=int, default=20)
parser.add_argument('--load_checkpoint', type=str, default=None)

args = parser.parse_args()

def visualize_memory_usage(model, time_series_data, seq_len, batch_size=1, elem_size=1, device='cuda'):
    def run_model(model, seq_len, batch_size, elem_size, device):
        with torch.no_grad():
            model.eval()
            prev_reads = model.initialize(batch_size, enable_tracking=True)

            test_inputs = time_series_data[-seq_len:].tolist()

            for i in range(seq_len):
                input_tensor = torch.FloatTensor([test_inputs[i]]).view(1, -1).to(device)
                _, prev_reads = model(input_tensor, prev_reads)

            y_out = torch.zeros(seq_len, batch_size, model.out.out_features).to(device)
            for t in range(seq_len):
                next_input = torch.zeros(batch_size, elem_size).to(device)
                y_out[t], prev_reads = model(next_input, prev_reads)

            read_weights, write_weights = model.get_tracked_weights()
            return read_weights, write_weights

    read_weights, write_weights = run_model(model, seq_len, batch_size, elem_size, device)

    plt.figure(figsize=(10, 6))

    # read_weights
    plt.subplot(1, 2, 1)
    plt.imshow(np.vstack([w.cpu() for w in read_weights]).T, aspect='auto', vmin=0., vmax=1.)
    plt.colorbar()
    plt.title('Read Weights')
    
    # write_weights
    plt.subplot(1, 2, 2)
    plt.imshow(np.vstack([w.cpu() for w in write_weights]).T, aspect='auto', vmin=0., vmax=1.)
    plt.colorbar()
    plt.title('Write Weights')

    plt.tight_layout()
    plt.savefig('memory_time_series.pdf')
    plt.show()

def validate_model(model, val_data, val_index_ranges, loss_function, seq_len, device='cuda'):
    model.eval()
    total_val_loss = 0.0

    with torch.no_grad():
        for start_idx, end_idx in val_index_ranges:
            input_seq = torch.FloatTensor(val_data[start_idx:end_idx]).view(end_idx - start_idx, 1, -1).to(device)
            target_seq = torch.FloatTensor(val_data[end_idx:end_idx + seq_len]).view(seq_len, 1, -1).to(device)

            prev_reads = model.initialize(1)

            for t in range(end_idx - start_idx):
                output, prev_reads = model(input_seq[t], prev_reads)

            y_out = torch.zeros(seq_len, 1, 1).to(device)
            for t in range(seq_len):
                output, prev_reads = model(output, prev_reads)
                y_out[t] = output

            val_loss = loss_function(y_out.view(-1, 1), target_seq.view(-1, 1))
            total_val_loss += val_loss.item()

    average_val_loss = total_val_loss / len(val_index_ranges)
    return average_val_loss

def generate_index_ranges(data_length, min_seq_len, max_seq_len, num_sequences):
    index_ranges = []
    for _ in range(num_sequences):
        start_idx = random.randint(0, data_length - max_seq_len - 1)
        current_seq_len = random.randint(min_seq_len, max_seq_len)
        end_idx = start_idx + current_seq_len
        index_ranges.append((start_idx, end_idx))
    return index_ranges

def train():
    dataset = get_rdataset("AirPassengers").data
    dataset['value'] = dataset['value'].astype(float)

    # We use scaler
    scaler = MinMaxScaler(feature_range=(-1, 1))
    data_normalized = scaler.fit_transform(dataset['value'].values.reshape(-1, 1))

    model = NTM(input_dim=1, 
            hidden_dim=args.hidden_dim,
            output_dim=1, 
            memory_size=args.memory_size,
            memory_cell_size=args.memory_cell_size,
            num_heads=args.num_heads,
            controller_layers=args.controller_layers,
            controller_clipping=args.controller_clipping,
            device=args.device, 
            dropout_rate=args.dropout_rate,
            use_sigmoid=False)
    
    if args.load_checkpoint:
        model.load_state_dict(torch.load(args.load_checkpoint))
    
    if args.optimizer == 'RMSProp':
        optimizer = torch.optim.RMSprop(model.parameters(), momentum=0.9, alpha=0.95, lr=args.learning_rate)
    elif args.optimizer == 'Adam':
        optimizer = torch.optim.Adam(model.parameters(), lr=args.learning_rate)
    elif args.optimizer == 'AdamW':
        optimizer = torch.optim.Adam(model.parameters(), lr=args.learning_rate)
    else:
        raise ValueError("Supported optimizers: RMSProp | Adam | AdamW")
    
    loss_function = nn.MSELoss()
    losses = []
    
    for epoch in range(args.num_epochs):
        total_index_ranges = generate_index_ranges(len(data_normalized) - (args.max_seq_len + args.pred_seq_len), args.min_seq_len, args.max_seq_len, args.train_num_sequences + int(0.2 * args.train_num_sequences))

        train_index_ranges = total_index_ranges[:args.train_num_sequences]
        val_index_ranges = total_index_ranges[args.train_num_sequences:]

        model.train()
        total_loss = 0.0

        for start_idx, end_idx in train_index_ranges:
            input_seq = torch.FloatTensor(data_normalized[start_idx:end_idx]).view(end_idx - start_idx, 1, -1).to(args.device)
            target_seq = torch.FloatTensor(data_normalized[start_idx:end_idx + args.pred_seq_len]).view(args.pred_seq_len + end_idx - start_idx, 1, -1).to(args.device)

            optimizer.zero_grad()
            prev_reads = model.initialize(1)

            y_out = torch.zeros(end_idx - start_idx + args.pred_seq_len, 1, 1).to(args.device)
            for t in range(end_idx - start_idx):
                output, prev_reads = model(input_seq[t], prev_reads)
                y_out[t] = output

            for t in range(args.pred_seq_len):
                output, prev_reads = model(output, prev_reads)
                y_out[end_idx - start_idx + t] = output

            loss = loss_function(y_out.view(-1, 1), target_seq.view(-1, 1))
            loss.backward()
            torch.nn.utils.clip_grad_norm_(model.parameters(), args.max_grad_norm)
            optimizer.step()

            total_loss += loss.item()

        if epoch % args.eval_frequency == 0 or epoch == args.num_epochs - 1:
            val_loss = validate_model(model, data_normalized, val_index_ranges, loss_function, args.max_seq_len, device=args.device)
            print(f'Epoch {epoch}, Average loss: {total_loss / len(train_index_ranges)}, Validation Loss: {val_loss}')
            losses.append(val_loss)

            if args.visualize_memory:
                visualize_memory_usage(model, torch.FloatTensor(data_normalized), args.max_seq_len, device=args.device)

            if args.visualize_prediction:
                predict(model, scaler)

    torch.save(model.state_dict(), "models/ntm_time_series.ckpt")
    filename = "data/ntm_time_series_losses.json"
    with open(filename, "w") as fp:
        json.dump(losses, fp)  # encode dict into JSON

    return model, scaler

def predict(model, scaler):
    fut_pred = 50
    seq_len = 20

    dataset = get_rdataset("AirPassengers").data
    dataset['value'] = dataset['value'].astype(float)

    data_normalized = scaler.transform(dataset['value'].values.reshape(-1, 1))
    
    test_inputs = data_normalized[-seq_len:].tolist()
    results = []
    
    model.eval()
    prev_reads = model.initialize(1)

    seq = torch.FloatTensor(np.array(test_inputs[-seq_len:])).view(seq_len, 1, -1).to(args.device)

    for t in range(seq_len):
        next_pred, prev_reads = model(seq[t], prev_reads)

    for _ in range(fut_pred):
        next_pred, prev_reads = model(next_pred, prev_reads)
        results.append(next_pred.cpu().detach().numpy().flatten())

    predicted_values = scaler.inverse_transform(np.array(results).reshape(-1, 1))

    # Plot the results
    plt.figure(figsize=(10,6))
    plt.ylabel('Total Passengers')
    plt.grid(True)
    plt.autoscale(axis='x', tight=True)
    plt.plot(dataset['value'], label='Data')
    plt.show()

    plt.plot(np.arange(len(dataset), len(dataset) + fut_pred), predicted_values, label='NTM', color='green')
    plt.show()

def main():
    if args.visualize_memory or args.visualize_prediction:
        print("WARNING: Visualizations are blocking - program will be paused when graphs are on screen!")
    model, scaler = train()
    predict(model, scaler)

if __name__ == "__main__":
    main()
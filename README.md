# Semestrální práce z NI-MVI

Čtěte `report.pdf` pro závěry práce.

## Zadání

**Bližší pohled na Neural Turing Machines**

Cílem práce je implementace architektury Neural Turing Machine a bližší prozkoumání jeho vlastností a praktického využití. V rámci práce bude model otestován na třech různých úlohách, v jejichž rámci bude pozorováno chování modelu při trénování i při jeho běhu. Úlohami jsou
 - kopírování vstupní sekvence
 - predikce časové řady
 - generování textu

## Běh programů

Výstupem práce není spustitelný program, ale jestliže máte zájem vidět vizualizace použité v reportu, načtěte příslušný checkpoint pomocí `--load_checkpoint <cesta_k_checkpointu>` a zapněte příslušné vizualizace pomocí `--visualize_<what> True` (např. `--visualize_memory True`). Model oběhne jednu epochu trénování a zobrazí grafy. Pár checkpointů je k dispozici v `models.zip`. Mějte na paměti, že se formát parametrů bude při použití `--memory_init_weights learned` lišit (váhy v `models.zip` nebudou fungovat). Seznam dostupných argumentů zobrazíte použitím přepínače `-h`. Implementace NTM je k dispozici v adresáři ntm. **Není třeba stahovat žádná data**, je nutné určitě mít nainstalovaný torch (viz. https://pytorch.org/get-started/locally/) a další package pak jednoduše doinstalujte podle PackageNotFoundError příkazem

```sh
pip install <package-name>
```

K trénování pro numerickou stabilitu použijte GPU (na CPU je to nestabilní) pomocí `--device cuda` (výchozí hodnota) a doporučuji příliš neměnit parametry modelu, zejm. inicializační schéma paměti. Pro více info čtěte `report.pdf`.

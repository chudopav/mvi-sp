import torch
import torch.nn as nn
import torch.optim as optim
import numpy as np
import unidecode
import argparse

from ntm.ntm import NTM

parser = argparse.ArgumentParser()
parser.add_argument("--seq_len", type=int, default=100)
parser.add_argument('--optimizer', type=str, default='RMSProp', help='RMSProp | Adam | AdamW')
parser.add_argument('--learning_rate', type=float, default=1e-2)
parser.add_argument('--num_epochs', type=int, default=20)
parser.add_argument('--max_grad_norm', type=float, default=10.)
parser.add_argument('--eval_frequency', type=int, default=5)
parser.add_argument('--hidden_dim', type=int, default=128)
parser.add_argument('--controller_layers', type=int, default=1)
parser.add_argument('--controller_clipping', type=float, default=20.)
parser.add_argument('--memory_size', type=int, default=128)
parser.add_argument('--memory_cell_size', type=int, default=20)
parser.add_argument('--memory_init_weights', type=str, default='ones', help='ones | learned | random')
parser.add_argument('--num_heads', type=int, default=1)
parser.add_argument('--max_shift', type=int, default=1)
parser.add_argument('--dropout_rate', type=int, default=0.3)
parser.add_argument('--device', type=str, default='cuda')
parser.add_argument('--batch_size', type=int, default=256)
parser.add_argument('--load_checkpoint', type=str, default=None)

args = parser.parse_args()

def load_data(filename):
    with open(filename, 'r', encoding='utf-8') as f:
        text = f.read()
    return unidecode.unidecode(text)

def one_hot_encode(arr, n_labels):
    one_hot = np.zeros((np.multiply(*arr.shape), n_labels), dtype=np.float32)
    one_hot[np.arange(one_hot.shape[0]), arr.flatten()] = 1.
    one_hot = one_hot.reshape((*arr.shape, n_labels))
    
    return one_hot

def get_batches(arr, batch_size, seq_length):
    batch_size_total = batch_size * seq_length
    n_batches = len(arr) // batch_size_total

    arr = arr[:n_batches * batch_size_total]
    arr = arr.reshape((batch_size, -1))

    for n in range(0, arr.shape[1], seq_length):
        x = arr[:, n:n+seq_length]
        y = np.zeros_like(x)
        try:
            y[:, :-1], y[:, -1] = x[:, 1:], arr[:, n+seq_length]
        except IndexError:
            y[:, :-1], y[:, -1] = x[:, 1:], arr[:, 0]
        yield x, y

def train():
    data = load_data('data/shakespeare.txt')
    chars = tuple(set(data))
    int2char = dict(enumerate(chars))
    char2int = {ch: ii for ii, ch in int2char.items()}
    encoded = np.array([char2int[ch] for ch in data])

    input_dim = len(chars)
    output_dim = input_dim

    model = NTM(input_dim=input_dim, 
            hidden_dim=args.hidden_dim,
            output_dim=output_dim, 
            memory_size=args.memory_size,
            memory_cell_size=args.memory_cell_size,
            num_heads=args.num_heads,
            controller_layers=args.controller_layers,
            controller_clipping=args.controller_clipping,
            device=args.device, 
            dropout_rate=args.dropout_rate)
    
    if args.load_checkpoint:
            model.load_state_dict(torch.load(args.load_checkpoint))
    
    if args.optimizer == 'RMSProp':
        optimizer = torch.optim.RMSprop(model.parameters(), momentum=0.9, alpha=0.95, lr=args.learning_rate)
    elif args.optimizer == 'Adam':
        optimizer = torch.optim.Adam(model.parameters(), lr=args.learning_rate)
    elif args.optimizer == 'AdamW':
        optimizer = torch.optim.Adam(model.parameters(), lr=args.learning_rate)
    else:
        raise ValueError("Supported optimizers: RMSProp | Adam | AdamW")
    
    criterion = nn.CrossEntropyLoss()

    losses = []

    for epoch in range(args.num_epochs):
        for x, y in get_batches(encoded, args.batch_size, args.seq_len):
            x = one_hot_encode(x, len(chars))
            x, y = torch.from_numpy(x).to(args.device), torch.from_numpy(y).to(args.device)

            model.zero_grad()
            prev_reads = model.initialize(args.batch_size)

            for i in range(args.seq_len):
                _, prev_reads = model(x[:, i, :], prev_reads)

            y_out = torch.zeros(args.seq_len, args.batch_size, output_dim).to(args.device)

            for i in range(args.seq_len):
                output, prev_reads = model(torch.zeros(args.batch_size, input_dim).to(args.device), prev_reads)
                y_out[i] = output

            y_out = y_out.transpose(0, 1).contiguous()  # swap batch and seq_length dimensions

            loss = criterion(y_out.view(-1, output_dim), y.view(-1).long())
            loss.backward()
            torch.nn.utils.clip_grad_norm_(model.parameters(), max_norm=args.max_grad_norm)
            losses.append(loss.item())
            optimizer.step()

        print(f'Epoch {epoch}/{args.num_epochs}, Loss: {loss.item()}')
        torch.save(model.state_dict(), f"models/ntm_shakespeare_epoch{epoch}.ckpt")

def main():
    train()

if __name__ == "__main__":
    main()


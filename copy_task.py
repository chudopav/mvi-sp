import torch
from torch import optim
from ntm.ntm import NTM
import random
import numpy as np
import matplotlib.pyplot as plt
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--min_seq_len", type=int, default=5)
parser.add_argument("--max_seq_len_start", type=int, default=20)
parser.add_argument("--max_seq_len_end", type=int, default=20)

# Simple curriculum learning
parser.add_argument("--seq_len_increase_frequency", type=int, default=10000)
parser.add_argument("--seq_len_increase_factor", type=int, default=2)

parser.add_argument('--optimizer', type=str, default='RMSProp', help='RMSProp | Adam | AdamW')
parser.add_argument('--learning_rate', type=float, default=5e-5)
parser.add_argument('--scheduler', type=bool, default=False)
parser.add_argument('--max_grad_norm', type=float, default=20.)
parser.add_argument('--num_epochs', type=int, default=100000)
parser.add_argument('--eval_frequency', type=int, default=50)
parser.add_argument('--batch_size', type=int, default=16)
parser.add_argument('--eval_batch_size', type=int, default=640)
parser.add_argument('--elem_size', type=int, default=8)
parser.add_argument('--hidden_dim', type=int, default=160)
parser.add_argument('--controller_layers', type=int, default=1)
parser.add_argument('--controller_clipping', type=float, default=20.)
parser.add_argument('--memory_size', type=int, default=128)
parser.add_argument('--memory_cell_size', type=int, default=20)
parser.add_argument('--memory_init_weights', type=str, default='ones', help='ones | learned | random')
parser.add_argument('--num_heads', type=int, default=1)
parser.add_argument('--max_shift', type=int, default=1)
parser.add_argument('--dropout_rate', type=int, default=0.)
parser.add_argument('--visualize_output', type=bool, default=False)
parser.add_argument('--visualize_memory', type=bool, default=False)
parser.add_argument('--enable_checkpoints', type=bool, default=False)
parser.add_argument('--device', type=str, default='cuda')
parser.add_argument('--load_checkpoint', type=str, default=None)

args = parser.parse_args()

def generate_sequence(seq_len, batch_size, elem_size, device='cpu'):
    seq = torch.bernoulli(torch.rand(seq_len, batch_size, elem_size)).to(device)

    seq_with_flag = torch.cat([seq, torch.zeros(seq_len, batch_size, 1).to(device)], dim=2)

    delimiter = torch.zeros(1, batch_size, elem_size + 1).to(device)
    delimiter[:, :, -1] = 1

    input_sequence = torch.cat([seq_with_flag, delimiter], dim=0)

    return input_sequence, seq

def compare_sequences(seq1, seq2):
    comparison_image = np.zeros((*seq1.shape, 3))  # Shape: [seq_len, elem_size, 3]
    similarities = seq1 == seq2
    for channel in range(3):  
        comparison_image[:, :, channel][similarities] = seq1[similarities]
    differences = seq1 != seq2
    comparison_image[differences, 0] = 1  # Red for differences
    return comparison_image

def visualize_model_output(model, seq_len, batch_size=1, elem_size=8, device='cuda'):
    def run_model(model, seq_len, batch_size, elem_size, device):
        with torch.no_grad():
            model.eval()
            prev_reads = model.initialize(batch_size)
            input_seq, target_seq = generate_sequence(seq_len, batch_size, elem_size, device)
            for elem in input_seq:
                _, prev_reads = model(elem, prev_reads)
            y_out = torch.zeros_like(target_seq).to(device)
            for idx in range(target_seq.size(0)):
                y_out[idx], prev_reads = model(torch.zeros(batch_size, elem_size + 1).to(device), prev_reads)

            return target_seq, y_out

    sequence1, sequence2 = run_model(model, seq_len, batch_size, elem_size, device)

    plt.figure(figsize=(5, 3 * batch_size))
    for i in range(batch_size):
        separated_seq = np.concatenate([
            sequence1[:, i, :].cpu().T, 
            np.ones((elem_size, 1)),  # Separator
            sequence2[:, i, :].cpu().T
        ], axis=1)

        plt.subplot(2 * batch_size, 1, 2 * i + 1)
        plt.imshow(separated_seq, cmap='gray', aspect='equal', vmin=0., vmax=1.)
        plt.axis('off')

        plt.subplot(2 * batch_size, 1, 2 * i + 2)
        comparison_image = compare_sequences(sequence1[:, i, :].cpu(), (sequence2 > 0.5).float()[:, i, :].cpu())
        plt.imshow(np.transpose(comparison_image, (1, 0, 2)), aspect='equal')
        plt.axis('off')

    plt.tight_layout()
    plt.show()

def visualize_memory_usage(model, seq_len, batch_size=1, elem_size=8, device='cuda'):
    def run_model(model, seq_len, batch_size, elem_size, device):
        with torch.no_grad():
            model.eval()
            prev_reads = model.initialize(batch_size, enable_tracking=True)
            input_seq, target_seq = generate_sequence(seq_len, batch_size, elem_size, device)
            for elem in input_seq:
                _, prev_reads = model(elem, prev_reads)
            y_out = torch.zeros_like(target_seq).to(device)
            for idx in range(target_seq.size(0)):
                y_out[idx], prev_reads = model(torch.zeros(batch_size, elem_size + 1).to(device), prev_reads)

            read_weights, write_weights = model.get_tracked_weights()
            return read_weights, write_weights

    read_weights, write_weights = run_model(model, seq_len, batch_size, elem_size, device)

    plt.figure(figsize=(10, 6))

    # read_weights
    plt.subplot(1, 2, 1)
    plt.imshow(np.vstack([w.cpu() for w in read_weights]).T, aspect='equal', vmin=0., vmax=1.)
    plt.axis('off')
    plt.title('Read Weights')
    
    # write_weights
    plt.subplot(1, 2, 2)
    plt.imshow(np.vstack([w.cpu() for w in write_weights]).T, aspect='equal', vmin=0., vmax=1.)
    plt.axis('off')
    plt.title('Write Weights')

    plt.tight_layout()
    plt.show()

def evaluate_model(model, seq_len, batch_size, elem_size, device):
    with torch.no_grad():
        model.eval()
        prev_reads = model.initialize(batch_size)
        input_seq, target_seq = generate_sequence(seq_len, batch_size, elem_size, device)
        for elem in input_seq:
            _, prev_reads = model(elem, prev_reads)

        y_out = torch.zeros_like(target_seq).to(device)
        for idx in range(target_seq.size(0)):
            y_out[idx], prev_reads = model(torch.zeros(batch_size, elem_size + 1).to(device), prev_reads)

        y_out_binarized = (y_out > 0.5).float()
        cost = torch.sum(torch.abs(y_out_binarized - target_seq)) / (target_seq.numel())
        return cost.item()

def train():
    model = NTM(input_dim=args.elem_size + 1, # One bit as control bit
            hidden_dim=args.hidden_dim,
            output_dim=args.elem_size, 
            memory_size=args.memory_size, 
            memory_cell_size=args.memory_cell_size,
            memory_weights=args.memory_init_weights,
            num_heads=args.num_heads,
            controller_layers=args.controller_layers,
            controller_clipping=args.controller_clipping,
            device=args.device,
            dropout_rate=args.dropout_rate)
    
    if args.load_checkpoint:
        model.load_state_dict(torch.load(args.load_checkpoint))

    if args.optimizer == 'RMSProp':
        optimizer = optim.RMSprop(model.parameters(), momentum=0.9, alpha=0.95, lr=args.learning_rate)
    elif args.optimizer == 'Adam':
        optimizer = optim.Adam(model.parameters(), lr=args.learning_rate)
    elif args.optimizer == 'AdamW':
        optimizer = optim.Adam(model.parameters(), lr=args.learning_rate)
    else:
        raise ValueError("Supported optimizers: RMSProp | Adam | AdamW")
    
    if args.scheduler:
        scheduler = optim.lr_scheduler.StepLR(optimizer, step_size=10000, gamma=0.5)

    loss_fn = torch.nn.BCELoss()
    min_seq_len = args.min_seq_len
    max_seq_len = args.max_seq_len_start
    generalization_costs = []

    for epoch in range(args.num_epochs):
        optimizer.zero_grad()
        model.train()

        # Simple curriculum learning
        if epoch % args.seq_len_increase_frequency == 0 and max_seq_len < args.max_seq_len_end:
            max_seq_len = min(max_seq_len * args.seq_len_increase_factor, args.max_seq_len_end)

        # Generate sequence
        current_seq_len = random.randint(min_seq_len, max_seq_len)

        input_seq, target_seq = generate_sequence(current_seq_len, args.batch_size, args.elem_size, args.device)
        prev_reads = model.initialize(args.batch_size)

        # Pass input
        for elem in input_seq:
            _, prev_reads = model(elem, prev_reads)

        # Produce output
        y_out = torch.zeros_like(target_seq).to(args.device)
        for idx in range(target_seq.size(0)):
            y_out[idx], prev_reads = model(torch.zeros(args.batch_size, args.elem_size + 1).to(args.device), prev_reads)

        # Backward apss
        loss = loss_fn(y_out, target_seq)
        loss.backward()

        # Gradient clipping
        torch.nn.utils.clip_grad_norm_(model.parameters(), max_norm=args.max_grad_norm)

        optimizer.step()

        if args.scheduler:
            scheduler.step()

        if epoch % args.eval_frequency == 0:
            generalization_cost = evaluate_model(model, args.max_seq_len_end, args.eval_batch_size, args.elem_size, device=args.device)
            generalization_costs.append(generalization_cost)

            print(f"Epoch {epoch}: generalization_cost = {generalization_cost}")

            if args.visualize_output:
                visualize_model_output(model, args.max_seq_len_end, elem_size=args.elem_size, device=args.device)

            if args.visualize_memory:
                visualize_memory_usage(model, args.max_seq_len_end, elem_size=args.elem_size, device=args.device)

            if args.enable_checkpoints:
                torch.save(model.state_dict(), f"models/ntm_copy_epoch{epoch}.ckpt")


    torch.save(model.state_dict(), "models/ntm_copy.ckpt")
    print("Model saved into \"models/ntm_copy.ckpt\"")

def main():
    if args.visualize_memory or args.visualize_output:
        print("WARNING: Visualizations are blocking - program will be paused when graphs are on screen!")
    train()

if __name__ == "__main__":
    main()
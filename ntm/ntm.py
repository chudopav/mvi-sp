import torch
from torch import nn
import torch.nn.functional as F

from .head import Head
from .memory import Memory
from .controller import LSTMController

class NTM(nn.Module):
    def __init__(self,
                 input_dim,
                 hidden_dim,
                 output_dim,
                 memory_size,
                 memory_cell_size,
                 memory_weights='ones',
                 num_heads=8,
                 max_shift=1,
                 dropout_rate=0.,
                 controller=None,
                 controller_layers=1,
                 controller_clipping=None,
                 device='cpu',
                 use_sigmoid=True):
        super(NTM, self).__init__()
        self.device = torch.device(device)
        self.use_sigmoid = use_sigmoid

        # Memory
        self.memory = Memory(memory_size, memory_cell_size, init_type=memory_weights, device=device)

        # Controller
        if controller is None:
            self.controller = LSTMController(input_dim + num_heads * memory_cell_size, hidden_dim, controller_layers, controller_clipping, device=device)
        else:
            self.controller = controller

        # Heads
        self.num_heads = num_heads
        self.heads = nn.ModuleList()
        for _ in range(num_heads):
            self.heads.append(Head('r', self.controller.output_dim, self.memory, max_shift=max_shift, device=device))
            self.heads.append(Head('w', self.controller.output_dim, self.memory, max_shift=max_shift, device=device))

        self.out = nn.Linear(hidden_dim, output_dim).to(self.device)
        nn.init.xavier_uniform_(self.out.weight, gain=1)
        nn.init.normal_(self.out.bias, std=0.01)
        self.dropout = nn.Dropout(dropout_rate)

    def initialize(self, batch_size, reset=True, enable_tracking=False):
        self.memory.initialize(batch_size)
        self.prev_state = self.controller.initialize(batch_size=batch_size)
        for head in self.heads:
            head.initialize(batch_size, reset, enable_tracking=enable_tracking)
        self.batch_size = batch_size
        if reset:
            prev_reads = torch.stack([head.init_r for head in self.heads if head.mode == 'r']).view(1, -1).expand(batch_size, -1).to(self.device)

        return prev_reads
    
    def get_tracked_weights(self):
        if len(self.heads) != 2:
            raise ValueError("Only supported for 1 reading and 1 writing head.")
        
        read_weights = self.heads[0].tracked_weights
        write_weights = self.heads[1].tracked_weights

        return read_weights, write_weights

    def forward(self, x, prev_reads):
        batch_size = x.size(0)
        x = x.to(self.device)
        prev_reads = prev_reads.view(batch_size, -1).to(self.device)
        controller_input = torch.cat((x, prev_reads), dim=1).to(self.device)
        controller_output = self.controller(controller_input)

        reads = torch.zeros((self.num_heads, self.batch_size, self.memory.n), device=self.device)
        for idx, head in enumerate(self.heads):
            if head.mode == 'r':
                read_vector = head(controller_output)
                reads[idx // 2] = read_vector
            elif head.mode == 'w':
                head(controller_output)

        controller_output = self.out(controller_output)
        controller_output = self.dropout(controller_output)
        
        if self.use_sigmoid:
            return F.sigmoid(controller_output), reads
        else:
            return controller_output, reads
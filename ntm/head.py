import torch
from torch import nn
import torch.nn.functional as F

def _convolve(w, s):
    assert s.size(0) == 3
    t = torch.cat([w[-1:], w, w[:1]], dim=0)
    c = F.conv1d(t.view(1, 1, -1), s.view(1, 1, -1)).view(-1)
    return c

class Head(nn.Module):
    """
    A head used to communicate with NTM memory.

    mode char 'r' for read; 'w' for write
    controller_output_dim int size of controller output
    memory Memory memory to read/write to/from
    max_shift maximal shift during convolutional shift
    """
    def __init__(self, mode, controller_output_dim, memory, max_shift=1, device='cpu'):
        super(Head, self).__init__()
        if mode not in ['r', 'w']:
            raise ValueError('The head mode must be \'r\' or \'w\'.')
        self.mode = mode
        self.max_shift = max_shift
        self.memory = memory
        self.device = torch.device(device)
        self.w_prev = None

        # Memory access
        self.k = nn.Linear(controller_output_dim, self.memory.n).to(self.device)
        self.beta = nn.Linear(controller_output_dim, 1).to(self.device)
        self.g = nn.Linear(controller_output_dim, 1).to(self.device)
        self.s = nn.Linear(controller_output_dim, 2 * max_shift + 1).to(self.device)
        self.gamma = nn.Linear(controller_output_dim, 1).to(self.device)

        # Write only
        if mode == 'w':
            self.add = nn.Linear(controller_output_dim, self.memory.n).to(self.device)
            self.erase = nn.Linear(controller_output_dim, self.memory.n).to(self.device)

        # Learnable parameters
        self.init_r = nn.Parameter(torch.abs(torch.randn(self.memory.n)) * 1e-3).to(self.device)
        self.init_w = nn.Parameter(torch.abs(torch.randn(self.memory.m)) * 1e-3).to(self.device)

        # Initialize weights and biases
        for layer in [self.k, self.beta, self.g, self.s, self.gamma]:
            nn.init.xavier_uniform_(layer.weight)
            nn.init.zeros_(layer.bias)
        if mode == 'w':
            for layer in [self.add, self.erase]:
                nn.init.xavier_uniform_(layer.weight)
                nn.init.zeros_(layer.bias)

        self.tracked_weights = None
            
    def initialize(self, batch_size, reset=True, enable_tracking=False):
        self.batch_size = batch_size
        if reset:
            self.w_prev = self.init_w.repeat(batch_size, 1)

        if enable_tracking:
            self.tracked_weights = []
        else:
            self.tracked_weights = None

    def forward(self, controller_output):
        k = F.tanh(self.k(controller_output))
        beta = F.softplus(self.beta(controller_output))
        g = F.sigmoid(self.g(controller_output))
        s = F.softmax(self.s(controller_output), dim=1)
        gamma = F.softplus(self.gamma(controller_output)) + 1

        w = self._obtain_weighting(k, beta, g, s, gamma)

        if self.mode == 'r':
            return self.memory.read(w)
        else:
            add = F.tanh(self.add(controller_output))
            erase = F.sigmoid(self.erase(controller_output))
            self.memory.write(w, erase, add)

        self.w_prev = w.detach().clone()
            
    def _obtain_weighting(self, k, beta, g, s, gamma):
        w = self.memory.content_addressing(k, beta)
        w = self._interpolation(w, g)
        w = self._convolutional_shift(w, s)
        w = self._sharpening(w, gamma)

        if self.tracked_weights is not None:
            self.tracked_weights.append(w)

        return w

    def _interpolation(self, w, g):
        return g * w + (1 - g) * self.w_prev

    def _convolutional_shift(self, w, s):
        result = w.clone()
        for b in range(len(w)):
            result[b] = _convolve(w[b], s[b])
        return result

    def _sharpening(self, w, gamma):
        w_pow = w.pow(gamma)
        return w_pow / torch.sum(w_pow, dim=1, keepdim=True)
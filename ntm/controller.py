import torch
from torch import nn
import torch.nn.functional as F

class LSTMController(nn.Module):
    def __init__(self, input_dim, hidden_dim, num_layers=1, clip_value=None, device='cpu'):
        super(LSTMController, self).__init__()
        self.hidden_dim = hidden_dim
        self.num_layers = num_layers
        self.output_dim = self.hidden_dim
        self.clip_value = clip_value
        self.device = torch.device(device)

        self.lstm = nn.LSTM(input_dim, hidden_dim, num_layers, batch_first=True).to(self.device)

        self._initialize_weights()

    def _initialize_weights(self):
        for name, param in self.lstm.named_parameters():
            if 'weight_ih' in name or 'weight_hh' in name:
                nn.init.orthogonal_(param)
            elif 'bias' in name:
                nn.init.zeros_(param)

    def initialize(self, batch_size):
        self.hidden = (torch.zeros(self.num_layers, batch_size, self.hidden_dim).to(self.device),
                       torch.zeros(self.num_layers, batch_size, self.hidden_dim).to(self.device))
        return self.hidden

    def forward(self, x):
        x = x.unsqueeze(1)

        out, self.hidden = self.lstm(x, self.hidden)

        if self.clip_value is not None:
            out = torch.clamp(out, -self.clip_value, self.clip_value)

        return out.squeeze(1)
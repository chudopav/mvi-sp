import torch
from torch import nn
import torch.nn.functional as F

class Memory(nn.Module):
    def __init__(self, m, n, init_type="ones", init_scaling=1e-6, device='cpu'):
        super(Memory, self).__init__()
        self.m = m
        self.n = n
        self.device = torch.device(device)

        if init_type == "ones":
            initial_memory = torch.ones(m, n) * init_scaling
        elif init_type == "random":
            initial_memory = torch.randn(m, n) * init_scaling
        elif init_type == "learned":
            initial_memory = torch.randn(m, n)
            nn.init.xavier_uniform_(initial_memory)

        if init_type == "learned":
            self.register_parameter('initial_memory', nn.Parameter(initial_memory.to(self.device)))
        else:
            self.register_buffer('initial_memory', initial_memory.to(self.device))

        self.memory = None

    def initialize(self, batch_size):
        self.batch_size = batch_size
        self.memory = self.initial_memory.clone().repeat(batch_size, 1, 1)

    def read(self, w):
        if self.memory is None:
            raise RuntimeError("initialize method must be called")
        
        return torch.matmul(w.unsqueeze(1), self.memory).squeeze(1)

    def write(self, w, e, a):
        if self.memory is None:
            raise RuntimeError("initialize method must be called")

        weights = w.unsqueeze(-1)
        erase = e.unsqueeze(1)
        add = a.unsqueeze(1)

        erased_memory = self.memory * (1 - torch.matmul(weights, erase))
        self.memory = erased_memory + torch.matmul(weights, add)

    def content_addressing(self, k, beta):
        if self.memory is None:
            raise RuntimeError("initialize method must be called")

        similarity = F.cosine_similarity(k.unsqueeze(1), self.memory, dim=2)
        w = F.softmax(beta * similarity, dim=1)

        return w